package metadata

import (
	"fmt"
	"net"
	"strings"
	"time"

	"github.com/Sirupsen/logrus"
	"github.com/rancher/external-dns/config"
	"github.com/rancher/external-dns/utils"
	"github.com/rancher/go-rancher-metadata/metadata"
)

const (
	metadataUrl = "http://rancher-metadata.rancher.internal/2015-12-19"
)

type MetadataClient struct {
	MetadataClient  metadata.Client
	EnvironmentName string
	EnvironmentUUID string
}

func getEnvironment(m metadata.Client) (string, string, error) {
	timeout := 30 * time.Second
	var err error
	var stack metadata.Stack
	for i := 1 * time.Second; i < timeout; i *= time.Duration(2) {
		stack, err = m.GetSelfStack()
		if err != nil {
			logrus.Errorf("Error reading stack info: %v...will retry", err)
			time.Sleep(i)
		} else {
			return stack.EnvironmentName, stack.EnvironmentUUID, nil
		}
	}
	return "", "", fmt.Errorf("Error reading stack info: %v", err)
}

func NewMetadataClient() (*MetadataClient, error) {
	m, err := metadata.NewClientAndWait(metadataUrl)
	if err != nil {
		logrus.Fatalf("Failed to configure rancher-metadata: %v", err)
	}

	envName, envUUID, err := getEnvironment(m)
	if err != nil {
		logrus.Fatalf("Error reading stack info: %v", err)
	}

	return &MetadataClient{
		MetadataClient:  m,
		EnvironmentName: envName,
		EnvironmentUUID: envUUID,
	}, nil
}

func (m *MetadataClient) GetVersion() (string, error) {
	return m.MetadataClient.GetVersion()
}

func (m *MetadataClient) GetMetadataDnsRecords() (map[string]utils.MetadataDnsRecord, error) {
	dnsEntries := make(map[string]utils.MetadataDnsRecord)
	err := m.getContainersDnsRecords(dnsEntries)
	if err != nil {
		return dnsEntries, err
	}
	return dnsEntries, nil
}

func (m *MetadataClient) getTraefikIps() ([]string, error) {
	traefikips := []string{}
	services, err := m.MetadataClient.GetServices()
	if err != nil {
		return nil, err
	}

	for _, service := range services {
		for _, container := range service.Containers {
			if len(container.ServiceName) == 0 || len(container.Ports) == 0 || !containerStateOK(container) {
				continue
			}
			if container.Labels["traefik.loadbalancer"] == "true" {
				hostUUID := container.HostUUID
				if len(hostUUID) == 0 {
					logrus.Debugf("Container's %v host_uuid is empty", container.Name)
					continue
				}
				host, err := m.MetadataClient.GetHost(hostUUID)
				if err != nil {
					logrus.Infof("%v", err)
					continue
				}

				ip, ok := host.Labels["io.rancher.host.external_dns_ip"]

				if !ok || ip == "" {
					ip = host.AgentIP
				}

				traefikips = append(traefikips, ip)
			}
		}
		logrus.Debugf("traefik ips %s", traefikips)
	}
	return traefikips, nil
}

func (m *MetadataClient) getContainersDnsRecords(dnsEntries map[string]utils.MetadataDnsRecord) error {
	services, err := m.MetadataClient.GetServices()
	if err != nil {
		return err
	}

	ourFqdns := make(map[string]struct{})
	hostMeta := make(map[string]metadata.Host)
	traefikips, err := m.getTraefikIps()
	if err != nil {
		return err
	}
	for _, service := range services {

		// Check for Service Label: io.rancher.service.external_dns
		// Accepts 'always', 'auto' (default), or 'never'
		policy, ok := service.Labels["io.rancher.service.external_dns"]
		if !ok {
			policy = "auto"
		} else if policy == "never" {
			logrus.Debugf("Service %v is Disabled", service.Name)
			continue
		}

		if service.Kind != "service" && service.Kind != "loadBalancerService" {
			continue
		}

		for _, container := range service.Containers {

			if ((len(container.Ports) == 0 && container.Labels["traefik.port"] == "") && policy != "always") || !containerStateOK(container) {
				continue
			}

			hostUUID := container.HostUUID
			if len(hostUUID) == 0 {
				logrus.Debugf("Container's %v host_uuid is empty", container.Name)
				continue
			}

			var host metadata.Host
			if _, ok := hostMeta[hostUUID]; ok {
				host = hostMeta[hostUUID]
			} else {
				host, err = m.MetadataClient.GetHost(hostUUID)
				if err != nil {
					logrus.Warnf("Failed to get host metadata: %v", err)
					continue
				}
				hostMeta[hostUUID] = host
			}

			// Check for Host Label: io.rancher.host.external_dns
			// Accepts 'true' (default) or 'false'
			if label, ok := host.Labels["io.rancher.host.external_dns"]; ok {
				if label == "false" {
					logrus.Debugf("Container %v Host %s is Disabled", container.Name, host.Name)
					continue
				}
			}

			var externalIP string
			if ip, ok := host.Labels["io.rancher.host.external_dns_ip"]; ok && len(ip) > 0 {
				externalIP = ip
			} else if len(container.Ports) > 0 {
				if ip, ok := parsePortToIP(container.Ports[0]); ok {
					externalIP = ip
				}
			}

			// fallback to host agent IP
			if len(externalIP) == 0 {
				logrus.Debugf("Fallback to host.AgentIP %s for container %s", host.AgentIP, container.Name)
				externalIP = host.AgentIP
			}

			if net.ParseIP(externalIP) == nil {
				logrus.Errorf("Skipping container %s: Invalid IP address %s", container.Name, externalIP)
			}

			nameTemplate, ok := service.Labels["io.rancher.service.external_dns_name_template"]
			if !ok {
				nameTemplate = config.NameTemplate
			}

			fqdn := utils.FqdnFromTemplate(nameTemplate, container.ServiceName, container.StackName,
				m.EnvironmentName, config.RootDomainName)
			if len(container.Labels["traefik.port"]) != 0 && len(traefikips) > 0{
				traefikFqdn := getTraefikFqdn(container)
				realFQDN := fqdn
				if traefikFqdn != "" {
					realFQDN = traefikFqdn
				}
				for _, record:= range traefikips {
					addToDnsEntries(realFQDN, record, container.ServiceName, container.StackName, dnsEntries)
				}
				// If we have a traefik.frontend.rule that is different that the fqdn
				// we also add external ip of container in dns with standard fqdn
				if len(container.Ports) > 0 && traefikFqdn != "" && traefikFqdn != fqdn {
					addToDnsEntries(fqdn, externalIP, container.ServiceName, container.StackName, dnsEntries)
				}
			}else {
				addToDnsEntries(fqdn, externalIP, container.ServiceName, container.StackName, dnsEntries)
			}
			ourFqdns[fqdn] = struct{}{}
		}
	}

	if len(ourFqdns) > 0 {
		stateFqdn := utils.StateFqdn(m.EnvironmentUUID, config.RootDomainName)
		stateRec := utils.StateRecord(stateFqdn, config.TTL, ourFqdns)
		dnsEntries[stateFqdn] = utils.MetadataDnsRecord{
			ServiceName: "",
			StackName:   "",
			DnsRecord:   stateRec,
		}
	}

	return nil
}

func getTraefikFqdn(container metadata.Container) (string) {
	if len(container.Labels["traefik.frontend.rule"]) != 0  && strings.HasPrefix(container.Labels["traefik.frontend.rule"],"Host:") {
		return utils.Fqdn(strings.TrimPrefix(container.Labels["traefik.frontend.rule"],"Host:"))
	}
	return ""
}

func addToDnsEntries(fqdn, ip, service, stack string, dnsEntries map[string]utils.MetadataDnsRecord) {
	var records []string
	if _, ok := dnsEntries[fqdn]; !ok {
		records = []string{ip}
	} else {
		records = dnsEntries[fqdn].DnsRecord.Records
		// skip if the records already have that IP
		for _, val := range records {
			if val == ip {
				return
			}
		}
		records = append(records, ip)
	}

	dnsEntries[fqdn] = utils.MetadataDnsRecord{
		ServiceName: service,
		StackName:   stack,
		DnsRecord:   utils.DnsRecord{fqdn, records, "A", config.TTL},
	}
}

func containerStateOK(container metadata.Container) bool {
	switch container.State {
	case "running":
	default:
		return false
	}

	switch container.HealthState {
	case "healthy":
	case "updating-healthy":
	case "":
	default:
		return false
	}

	return true
}

// expects port string as 'ip:publicPort:privatePort'
// returns usable ip address
func parsePortToIP(port string) (string, bool) {
	parts := strings.Split(port, ":")
	if len(parts) == 3 {
		ip := parts[0]
		if len(ip) > 0 && ip != "0.0.0.0" {
			return ip, true
		}
	}

	return "", false
}
