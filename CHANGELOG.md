# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.7.2-traefik-34] - 2017-08-16
### Fixed
- traefik fqdn do not end with a dot

## [0.7.2-traefik-3] - 2017-08-16
### Added
- use traefik host rule to update dns

### Fixed
- fix issue that added the host in dns even without a port

## [0.7.2-traefik-2] - 2017-08-15
### Added
- traefik + external dns can cohabit on same service 

## [0.7.2-traefik] - 2017-07-15
### Added
- traefik support (use traefik.loadbalancer=true label on your traefik instances)
